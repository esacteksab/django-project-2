#from web_site.conf import base

from UserProfile.models import UserProfile


def navArtists(request):
    ctx = {
        "artistNav": UserProfile.objects.filter(artist=True),
    }
    return ctx
