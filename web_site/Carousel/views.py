from web_site.Carousel.models import CarouselPicture, CarouselPictureForm
from web_site.Community.models import CommunityEvent
from web_site.News.models import NewsArticle
from web_site.Press.models import PressArticle
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, DeleteView
from django.http import HttpResponse
from django.utils import simplejson
from django.core.urlresolvers import reverse


def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"


class CarouselPictureCreateView(CreateView):
    model = CarouselPicture
    template_name = "Carousel/carousel_picture_form.html"
    form_class = CarouselPictureForm

    def form_valid(self, form, **kwargs):
        obj = form.save(commit=False)

        obj.save()

        data = [
            {
            'name': obj.file.name,
            'url': obj.file.url,
            'thumbnail_url': obj.file.url,
            'delete_url': reverse('carousel-upload-delete', args=[obj.id]),
            'delete_type': "DELETE"
            }
        ]

        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class CarouselPictureDeleteView(DeleteView):
    model = CarouselPicture

    def get_context_data(self, **kwargs):
        context = super(CarouselPictureDeleteView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']
        return context

    def delete(self, request, *args, **kwargs):
        """
        This does not actually delete the file, only the database record.  But
        that is easy to implement.
        """
        self.object = self.get_object()
        self.object.delete()
        if request.is_ajax():
            response = JSONResponse(True, {}, response_mimetype(self.request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        else:

            return redirect('admini-display-carousel-images')


class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self,obj='',json_opts={},mimetype="application/json",*args,**kwargs):
        content = simplejson.dumps(obj,**json_opts)
        super(JSONResponse,self).__init__(content,mimetype,*args,**kwargs)


def ShopHomePage(request):
    carouselImages = CarouselPicture.objects.all()
    events = CommunityEvent.objects.all()[:2]
    press = PressArticle.objects.all()[:2]
    news = NewsArticle.objects.all()[:2]
    template = "shop-homepage.html"
    data = {
        'carouselImages': carouselImages,
        'events': events,
        'press': press,
        'news': news,
    }
    return render(request, template, data)