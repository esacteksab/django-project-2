from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from web_site.Carousel.views import CarouselPictureCreateView, CarouselPictureDeleteView
from web_site.Carousel.models import CarouselPicture

urlpatterns = patterns("",
	### Carousel admin
    ###
    ###
    ### Display Carousel Images
    url(r'^$', login_required(ListView.as_view(
        model=CarouselPicture,
        template_name="account/carousel_images.html")),
        name="admini-display-carousel-images"),
    ### Add Carousel Images
    url(r'^images/add/$', login_required(CarouselPictureCreateView.as_view(),
        {}),
        name="add-carousel-images"),
    ### Delete community event images
    url(r'^images/(?P<pk>\d+)/delete/$',
        login_required(CarouselPictureDeleteView.as_view()),
            name="carousel-upload-delete"),

)