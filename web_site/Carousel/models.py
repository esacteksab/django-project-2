from django.db import models
from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django_thumbs.db.models import ImageWithThumbsField
from south.modelsinspector import add_introspection_rules

add_introspection_rules(
    [
        (
            (ImageWithThumbsField, ),
            [],
            {
                "verbose_name": ["verbose_name", {"default": None}],
                "name":         ["name",         {"default": None}],
                "width_field":  ["width_field",  {"default": None}],
                "height_field": ["height_field", {"default": None}],
                "sizes":        ["sizes",        {"default": None}],
            },
        ),
    ],
    ["^django_thumbs.db.models", ])


class CarouselPicture(models.Model):
    file = ImageWithThumbsField(upload_to="carousel", sizes=((125, 125), (250, 250), (640, 427),))
    slug = models.SlugField(max_length=50, blank=True)

    def __unicode__(self):
        return '%s' % (self.file)

    @models.permalink
    def get_absolute_url(self):
        return('admin-carousel-images', {
            'pk': self.id
            })

    def save(self, *args, **kwargs):
        self.slug = self.file.name
        super(CarouselPicture, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.file.delete(False)
        super(CarouselPicture, self).delete(*args, **kwargs)


class CarouselPictureForm(forms.ModelForm):
    class Meta:
        model = CarouselPicture
        fields = ['file', ]

    def UploadImage(request):
        """Upload Images"""
        if request.method == 'POST':
            form = CarouselPictureForm(request.POST, request.FILES)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                return HttpResponseRedirect(".")
        else:
            form = CarouselPictureForm()
        template = "carousel_picture_form.html"
        data = {
            'form': form,
        }
        return render_to_response(template, data, context_instance=RequestContext(request))