# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CarouselPicture'
        db.create_table('Carousel_carouselpicture', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='file', sizes=((125, 125), (250, 250), (640, 427)))),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, blank=True)),
        ))
        db.send_create_signal('Carousel', ['CarouselPicture'])


    def backwards(self, orm):
        # Deleting model 'CarouselPicture'
        db.delete_table('Carousel_carouselpicture')


    models = {
        'Carousel.carouselpicture': {
            'Meta': {'object_name': 'CarouselPicture'},
            'file': ('django_thumbs.db.models.ImageWithThumbsField', [], {'max_length': '100', 'name': "'file'", 'sizes': '((125, 125), (250, 250), (640, 427))'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'})
        }
    }

    complete_apps = ['Carousel']