from django.db import models
from django import forms
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django_thumbs.db.models import ImageWithThumbsField
from south.modelsinspector import add_introspection_rules

add_introspection_rules(
    [
        (
            (ImageWithThumbsField, ),
            [],
            {
                "verbose_name": ["verbose_name", {"default": None}],
                "name":         ["name",         {"default": None}],
                "width_field":  ["width_field",  {"default": None}],
                "height_field": ["height_field", {"default": None}],
                "sizes":        ["sizes",        {"default": None}],
            },
        ),
    ],
    ["^django_thumbs.db.models", ])


class Picture(models.Model):

    user = models.ForeignKey(User)
    file = ImageWithThumbsField(upload_to="pictures", sizes=((125, 125), (250, 250), (427, 640),))
    slug = models.SlugField(max_length=50, blank=True)
    caption = models.TextField(blank=True, null=True)
    title = models.CharField(max_length=64, blank=True)
    carousel = models.BooleanField()

    def __unicode__(self):
        return '%s' % (self.file)

    @models.permalink
    def get_absolute_url(self, *args, **kwargs):
        return('admin-display-artist-images', (), {
            'pk': self.user.id
            })

    def save(self, *args, **kwargs):
        self.slug = self.file.name
        super(Picture, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.file.delete(False)
        super(Picture, self).delete(*args, **kwargs)


class PictureForm(forms.ModelForm):
    class Meta:
        model = Picture
        fields = ['file', ]

    def UploadImage(request):
        """Upload Images"""
        if request.method == 'POST':
            form = PictureForm(request.POST, request.FILES)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.user = request.user
                obj.caption = ''
                obj.title = ''
                obj.save()
                return HttpResponseRedirect(".")
        else:
            form = PictureForm()
        template = "picture_form.html"
        data = {
            'form': form,
        }
        return render_to_response(template, data, context_instance=RequestContext(request))


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    displayName = models.CharField(max_length=75)
    instagram = models.CharField(max_length=255, blank=True, null=True)
    instagram_url = models.CharField(max_length=255)
    bio = models.TextField()
    artist = models.BooleanField()
    sort = models.PositiveSmallIntegerField(max_length=4, null=False, blank=False)
    slug = models.SlugField(max_length=75, blank=True, null=True)

    class Meta:
        ordering = ['sort']

    def __unicode__(self):
        return '%s' '%s' '%s' '%s' % (self.user, self.instagram, self.bio, self.artist)

    def save(self, *args, **kwargs):
        self.slug = self.displayName.rstrip().replace(' ', '-').replace('"', '')
        self.instagram_url = self.instagram.lstrip('@')
        super(UserProfile, self).save(*args, **kwargs)
