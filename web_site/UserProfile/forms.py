from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *
from web_site.UserProfile.models import UserProfile, Picture


class ArtistForm(ModelForm):
    """A form to allow the management of Artists"""
    class Meta:
        model = UserProfile
        fields = ['user', 'displayName', 'instagram', 'bio', 'artist', 'sort']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'artist'
        self.helper.form_class = 'form-inline'
        self.helper.form_action = ''
        self.helper.layout = Layout(

            Field('user', css_class="input-large"),
            Field('displayName', css_class="input-large"),
            Field('instagram', css_class="input-large"),
            Field('bio', css_class="input-large"),
            Field('artist', css_class="input-large"),
            Field('sort', class_class="input-large"),

        FormActions(
            Submit('submit', "Submit", css_class='btn'),
            Submit('cancel', "Cancel", css_class='btn'),
        )
    )
        super(ArtistForm, self).__init__(*args, **kwargs)


class ArtistPictureForm(ModelForm):
    """ Edit an Artist's Image"""
    class Meta:
        model = Picture
        fields = ['file', 'caption', 'title', 'carousel']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'artist-image'
        self.helper.form_class = 'form-inline'
        self.helper.form_action = ''
        self.helper.layout = Layout(

            Field('title', css_class="input-large"),
            Field('caption', css_class="input-large"),
            Field('file'),
            Field('carousel'),

        FormActions(
            Submit('submit', "Submit", css_class='btn'),
            Submit('cancel', "Cancel", css_class='btn'),
        )

    )
        super(ArtistPictureForm, self).__init__(*args, **kwargs)
