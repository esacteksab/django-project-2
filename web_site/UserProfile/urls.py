from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DeleteView, UpdateView
from web_site.UserProfile.models import UserProfile, Picture
from web_site.UserProfile.views import PictureCreateView, PictureDeleteView, AdminGetArtistImages
from web_site.UserProfile.forms import ArtistForm, ArtistPictureForm
from web_site.UserProfile.views import PublicGetArtistImages, DisplayAristImagesAdmin, AdminArtistImageUpdatePageView, AdminArtistImageDeleteView

urlpatterns = patterns("",
	### Artists admin
    ###
    ###
    ### Display Arists in Admin
    url(r'^$', login_required(ListView.as_view(
        model=UserProfile,
        queryset=UserProfile.objects.filter(artist=True),
        template_name="account/artists_admin.html"))),

    ### Edit an artist profile
    url(r'^edit/(?P<pk>\d+)/$', login_required(UpdateView.as_view(
        model=UserProfile,
        form_class=ArtistForm,
        success_url=("/account/artists"),
        template_name="account/artist_admin.html",
        ))),
    ### Delete an artist profile
    url(r'^delete/(?P<pk>\d+)/$', login_required(DeleteView.as_view(
        model=UserProfile,
        success_url=('/account/artists')))),

    ### Display an Artist's images
    url(r'^(?P<pk>\d+)/images$', login_required(AdminGetArtistImages.as_view(
        template_name="account/admin_account_images.html"
        )),
        name="admin-display-artist-images"),

    ### Edit an Artist's image
    url(r'^(?P<id>\d+)/images/image/(?P<pk>\d+)/edit/$', login_required(AdminArtistImageUpdatePageView.as_view(
        form_class=ArtistPictureForm,
        template_name="account/artist_images_admin.html")),
        name="admin-artist-image-edit"),
    ### Delete an Artists's Image
    url(r'^(?P<id>\d+)/images/image/(?P<pk>\d+)/delete/$',
        login_required(AdminArtistImageDeleteView.as_view()), {},
            name="press-upload-delete"),
)