from web_site.UserProfile.models import Picture, PictureForm, UserProfile
from django.views.generic import CreateView, DeleteView, UpdateView
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from django.utils import simplejson
from django.core.urlresolvers import reverse
from django.views.generic import ListView


def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"


class PictureCreateView(CreateView):
    model = Picture
    template_name = "account/upload.html"
    form_class = PictureForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        u = self.request.POST.get("profileid")

        data = [
            {
            'user': u,
            'name': obj.file.name,
            'url': obj.file.url,
            'thumbnail_url': obj.file.url,
            'delete_url': reverse('delete-artist-images', args=[obj.id]),
            'delete_type': "DELETE"
            }
        ]

        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class PictureDeleteView(DeleteView):
    model = Picture

    def delete(self, request, *args, **kwargs):
        """
        This does not actually delete the file, only the database record.  But
        that is easy to implement.
        """
        self.object = self.get_object()
        self.object.delete()
        if request.is_ajax():
            response = JSONResponse(True, {}, response_mimetype(self.request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        else:

            return redirect('manage-artist-images',)


class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self,obj='',json_opts={},mimetype="application/json",*args,**kwargs):
        content = simplejson.dumps(obj,**json_opts)
        super(JSONResponse,self).__init__(content,mimetype,*args,**kwargs)


def ListArtists(request):
    return render_to_response('shop-homepage.html', {},
        context_instance=RequestContext(request))


class GetArtistImages(ListView):
    queryset = UserProfile.objects.filter(artist=True)
    allow_empty = False

    def get_context_data(self, **kwargs):
        context = super(GetArtistImages, self).get_context_data(**kwargs)
        context['image_list'] = Picture.objects.all()
        return context

    def get_queryset(self):
        return Picture.objects.filter(user__id=self.kwargs['pk'])


class PublicGetArtistImages(ListView):
    queryset = UserProfile.objects.filter(artist=True)
    #allow_empty = False

    def get_context_data(self, **kwargs):
        context = super(PublicGetArtistImages, self).get_context_data(**kwargs)
        context['image_list'] = Picture.objects.filter(user__userprofile__slug=self.kwargs['slug'])
        return context

    def get_queryset(self):
        return UserProfile.objects.filter(user__userprofile__slug=self.kwargs['slug'])


class AdminGetArtistImages(ListView):
    queryset = UserProfile.objects.filter(artist=True)
    allow_empty = True

    def get_context_data(self, **kwargs):
        context = super(AdminGetArtistImages, self).get_context_data(**kwargs)
        context['image_list'] = Picture.objects.all()
        return context

    def get_queryset(self):
        return Picture.objects.filter(user__id=self.kwargs['pk'])


class DisplayAristImagesAdmin(ListView):
    queryset = UserProfile.objects.filter(artist=True)
    allow_empty = True

    def get_queryset(self):
        return Picture.objects.filter(user=self.request.user)


class AdminArtistImageUpdatePageView(UpdateView):
    model = Picture

    def get_context_data(self, **kwargs):
        context = super(AdminArtistImageUpdatePageView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']

        return context

    def update(self, request, *args, **kwargs):

        return redirect('admin-display-artist-images', pk=kwargs['id'])


class AdminArtistImageDeleteView(DeleteView):
    model = Picture

    def delete(self, request, *args, **kwargs):
        """
        This does not actually delete the file, only the database record.  But
        that is easy to implement.
        """
        self.object = self.get_object()
        self.object.delete()
        if request.is_ajax():
            response = JSONResponse(True, {}, response_mimetype(self.request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        else:

            return redirect('admin-display-artist-images', pk=kwargs['id'])
