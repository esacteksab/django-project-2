from django.contrib import admin
from web_site.UserProfile.models import Picture, UserProfile

admin.site.register(Picture)
admin.site.register(UserProfile)
