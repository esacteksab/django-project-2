import re
from django.db import models
from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django_thumbs.db.models import ImageWithThumbsField
from south.modelsinspector import add_introspection_rules

add_introspection_rules(
    [
        (
            (ImageWithThumbsField, ),
            [],
            {
                "verbose_name": ["verbose_name", {"default": None}],
                "name":         ["name",         {"default": None}],
                "width_field":  ["width_field",  {"default": None}],
                "height_field": ["height_field", {"default": None}],
                "sizes":        ["sizes",        {"default": None}],
            },
        ),
    ],
    ["^django_thumbs.db.models", ])


class Distributor(models.Model):
    Name = models.CharField(max_length=255)
    URL = models.URLField(verify_exists=False)

    class Meta:
        ordering = ['Name']

    def __unicode__(self):
        return "'%s' | '%s'" % (self.Name, self.URL)


class Product(models.Model):
    Name = models.CharField(max_length=255)
    Description = models.TextField()
    PriImage = ImageWithThumbsField(upload_to="images/", blank=True, null=True,
        sizes=((125, 125), (250, 250), (640, 427),))
    distributors = models.ManyToManyField('Distributor', null=True, blank=True)
    slug = models.SlugField(null=True, blank=True)

    def __unicode__(self):
        return '%s' '%s' '%s' '%s' % (self.Name, self.Description, self.PriImage, self.distributors)

    def save(self, *args, **kwargs):
        self.slug = re.sub(r'\W+', '', self.Name)
        super(Product, self).save(*args, **kwargs)


class ProductImage(models.Model):
    file = ImageWithThumbsField(upload_to="images/",
        sizes=((125, 125), (250, 250), (640, 427),))
    slug = models.SlugField(max_length=50, blank=True)
    product = models.ForeignKey(Product)

    def __unicode__(self):
        return '%s' % (self.file)

    @models.permalink
    def get_absolute_url(self, *args, **kwargs):
        return('product-images', (), {
            'pk': self.product.id
            })

        def save(self, *args, **kwargs):
            self.slug = self.file.name
            super(ProductImage, self).save(*args, **kwargs)

        def delete(self, *args, **kwargs):
            self.file.delete(False)
            super(ProductImage, self).delete(*args, **kwargs)


class ProductImageForm(forms.ModelForm):
    class Meta:
        model = ProductImage
        fields = ['file', ]

    def UploadImage(request):
        """Upload Images"""
        if request.method == 'POST':
            form = ProductImageForm(request.POST, request.FILES)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.product = form.kwargs['pk']
                obj.save()
                return HttpResponseRedirect(".")
        else:
            form = ProductImageForm()
        template = "account/admin_product_image_form.html"
        data = {
            'form': form,
        }
        return render_to_response(template, data, context_instance=RequestContext(request))
