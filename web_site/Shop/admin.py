from django.contrib import admin
from web_site.Shop.models import Distributor, Product, ProductImage

admin.site.register(Distributor)
admin.site.register(Product)
admin.site.register(ProductImage)
