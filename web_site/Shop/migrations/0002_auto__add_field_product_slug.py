# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Product.slug'
        db.add_column('Shop_product', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default=0, max_length=50),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Product.slug'
        db.delete_column('Shop_product', 'slug')


    models = {
        'Shop.distributor': {
            'Meta': {'object_name': 'Distributor'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'URL': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Shop.product': {
            'Description': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'Product'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'PriImage': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'PriImage'", 'sizes': '((125, 125), (250, 250), (427, 640))', 'max_length': '100', 'blank': 'True', 'null': 'True'}),
            'distributors': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['Shop.Distributor']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        'Shop.productimage': {
            'Meta': {'object_name': 'ProductImage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Shop.Product']"}),
            'productImage': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'productImage'", 'sizes': '((125, 125), (250, 250), (427, 640))', 'max_length': '100', 'blank': 'True', 'null': 'True'})
        }
    }

    complete_apps = ['Shop']