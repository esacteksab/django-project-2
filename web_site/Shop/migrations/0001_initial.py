# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Distributor'
        db.create_table('Shop_distributor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('URL', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal('Shop', ['Distributor'])

        # Adding model 'Product'
        db.create_table('Shop_product', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('Description', self.gf('django.db.models.fields.TextField')()),
            ('PriImage', self.gf('django_thumbs.db.models.ImageWithThumbsField')(name='PriImage', sizes=((125, 125), (250, 250), (427, 640)), max_length=100, blank=True, null=True)),
        ))
        db.send_create_signal('Shop', ['Product'])

        # Adding M2M table for field distributors on 'Product'
        db.create_table('Shop_product_distributors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('product', models.ForeignKey(orm['Shop.product'], null=False)),
            ('distributor', models.ForeignKey(orm['Shop.distributor'], null=False))
        ))
        db.create_unique('Shop_product_distributors', ['product_id', 'distributor_id'])

        # Adding model 'ProductImage'
        db.create_table('Shop_productimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('productImage', self.gf('django_thumbs.db.models.ImageWithThumbsField')(name='productImage', sizes=((125, 125), (250, 250), (427, 640)), max_length=100, blank=True, null=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Shop.Product'])),
        ))
        db.send_create_signal('Shop', ['ProductImage'])


    def backwards(self, orm):
        # Deleting model 'Distributor'
        db.delete_table('Shop_distributor')

        # Deleting model 'Product'
        db.delete_table('Shop_product')

        # Removing M2M table for field distributors on 'Product'
        db.delete_table('Shop_product_distributors')

        # Deleting model 'ProductImage'
        db.delete_table('Shop_productimage')


    models = {
        'Shop.distributor': {
            'Meta': {'object_name': 'Distributor'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'URL': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Shop.product': {
            'Description': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'Product'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'PriImage': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'PriImage'", 'sizes': '((125, 125), (250, 250), (427, 640))', 'max_length': '100', 'blank': 'True', 'null': 'True'}),
            'distributors': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['Shop.Distributor']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Shop.productimage': {
            'Meta': {'object_name': 'ProductImage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Shop.Product']"}),
            'productImage': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'productImage'", 'sizes': '((125, 125), (250, 250), (427, 640))', 'max_length': '100', 'blank': 'True', 'null': 'True'})
        }
    }

    complete_apps = ['Shop']