import floppyforms as forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *
from web_site.Shop.models import Distributor, Product, ProductImage


class ShopDistributorForm(forms.ModelForm):
    """A partial form to allow the association to products"""
    class Meta:
        model = Distributor
        fields = ['Name', 'URL']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'manage-product'
        self.helper.form_class = 'well form-inline'
        self.helper.form_action = ''
        self.helper.layout = Layout(
            Field('Name'),
            Field('URL'),
        FormActions(
            Submit('submit', "Submit", css_class='btn'),
            Submit('cancel', "Cancel"),
        )


    )
        super(ShopDistributorForm, self).__init__(*args, **kwargs)


class ShopProductForm(forms.ModelForm):
    """A partial form to allow the association to distributors"""
    class Meta:
        model = Product
        widgets = {'distributors': forms.widgets.CheckboxSelectMultiple}
        fields = ['Name', 'Description', 'PriImage', 'distributors']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_action = ''
        self.helper.form_id = 'manage-product'
        self.helper.form_class = 'well form-inline'
        self.helper.layout = Layout(
            Field('Name'),
            Field('Description'),
            Field('PriImage',),
            Field('distributors'),
        FormActions(
            Submit('submit', "Submit", css_class='btn'),
            Submit('cancel', "Cancel"),
        )
    )

        super(ShopProductForm, self).__init__(*args, **kwargs)


#  This form may turn into jquery upload form
# class ProductImageForm(forms.ModelForm):
#     """A form to allow the addition of product images"""
#     class Meta:
#         model = ProductImage
#         fields = ['productImage']

#     def __init__(self, *args, **kwargs):
#         self.helper = FormHelper()
#         self.helper.form_method = 'POST'
#         self.helper.form_action = ''
#         self.helper.form_id = 'manage-product-images'
#         self.helper.form_class = 'well form-inline'

#         self.helper.layout = Layout(

#             Field('productImage'),

#         FormActions(
#             Submit('submit', "Submit", css_class='btn'),
#             Submit('cancel', "Cancel"),
#         )
#     )
#         super(ProductImageForm, self).__init__(*args, **kwargs)
