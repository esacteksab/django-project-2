# Create your views here.
from django.views.generic import CreateView, DeleteView
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils import simplejson
#from web_site.Shop.forms import ProductImageForm
from web_site.Shop.models import Product, Distributor, ProductImage, ProductImageForm
from django.views.generic import ListView


def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"


class ProductImageCreateView(CreateView):
    model = ProductImage
    template_name = "account/product-image-upload.html"
    form_class = ProductImageForm

    def form_valid(self, form, **kwargs):
        obj = form.save(commit=False)
        obj.product_id = self.kwargs['pk']
        obj.save()

        data = [
            {
            'name': obj.file.name,
            'url': obj.file.url,
            'thumbnail_url': obj.file.url,
            'delete_url': reverse('product-image-delete', kwargs={'id':self.kwargs['pk'], 'pk': obj.id}),
            'delete_type': "DELETE"
            }
        ]

        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class ProductImageDeleteView(DeleteView):
    model = ProductImage

    def get_context_data(self, **kwargs):
        context = super(ProductImageDeleteView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']
        return context

    def delete(self, request, *args, **kwargs):
        """
        This does not actually delete the file, only the database record.  But
        that is easy to implement.
        """
        self.object = self.get_object()
        self.object.delete()
        if request.is_ajax():
            response = JSONResponse(True, {}, response_mimetype(self.request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        else:

            return redirect('product-images', pk=kwargs['id'])


class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self, obj='', json_opts={}, mimetype="application/json", *args, **kwargs):
        content = simplejson.dumps(obj, **json_opts)
        super(JSONResponse, self).__init__(content, mimetype, *args, **kwargs)


def displayMerchandise(request):
    merchandise = Product.objects.all()
    template = "merchandise-homepage.html"
    data = {
        'merchandise': merchandise,
    }
    return render(request, template, data)


def productManagement(request):
    products = Product.objects.all()
    distributors = Distributor.objects.all()
    template = "account/admin-products.html"
    data = {
        'products': products,
        'distributors': distributors,
    }
    return render(request, template, data)


class viewProduct(ListView):

    def get_context_data(self, **kwargs):
        context = super(viewProduct, self).get_context_data(**kwargs)
        context['slug'] = self.kwargs['slug']
        return context

    def get_queryset(self):
        return Product.objects.filter(slug=self.kwargs['slug'])


class ProductImageListView(ListView):

    def get_context_data(self, **kwargs):
        context = super(ProductImageListView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']
        return context

    def get_queryset(self):
        return ProductImage.objects.filter(product__id=self.kwargs['pk'])
