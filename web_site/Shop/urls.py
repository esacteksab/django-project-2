from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.views.generic import DeleteView, UpdateView, CreateView

from web_site.Shop.models import Product, Distributor
from web_site.Shop.forms import ShopProductForm, ShopDistributorForm
from web_site.Shop.views import ProductImageListView, ProductImageDeleteView, ProductImageCreateView


urlpatterns = patterns("",
    ## Display All Products
    url(r'^$', 'web_site.Shop.views.productManagement',
        name="display-products"),
    ## Add a product
    url(r'^add/$', login_required(
        CreateView.as_view(
            model=Product,
            form_class=ShopProductForm,
            success_url=("/account/product"),
            template_name="account/admin_product_distributor_form.html")),
            name="add-product"),
    ## Update a product
    url(r'^edit/(?P<pk>\d+)/$', login_required(
        UpdateView.as_view(
            model=Product,
            form_class=ShopProductForm,
            success_url=("/account/product"),
            template_name="account/admin_product_distributor_form.html")),
            name="update-product"),
    ## Delete a product
    url(r'^delete/(?P<pk>\d+)/$', login_required(
        DeleteView.as_view(
            model=Product,
            success_url=("/account/product"))),
            name="delete-product"),


    url(r'^(?P<pk>\d+)/images/$', login_required(
        ProductImageListView.as_view(
            template_name="account/admin_product_images.html")),
            name="product-images"),
    url(r'^(?P<pk>\d+)/images/add/$', login_required(
        ProductImageCreateView.as_view(),
            {}),
            name="add-product-images"),
    url(r'^(?P<id>\d+)/images/image/(?P<pk>\d+)/delete/$',
        login_required(ProductImageDeleteView.as_view()), {},
            name="product-image-delete"),


    ## Edit a Distributor
    url(r'^distributor/add/$', login_required(
        CreateView.as_view(
            model=Distributor,
            form_class=ShopDistributorForm,
            success_url=("/account/product"),
            template_name="account/admin_distributor_form.html")),
            name="add-distributor"),
    url(r'^distributor/edit/(?P<pk>\d+)/$', login_required(
        UpdateView.as_view(
            model=Distributor,
            form_class=ShopDistributorForm,
            success_url=("/account/product"),
            template_name="account/admin_distributor_form.html")),
            name="create-distributor"),
    url(r'^distributor/delete/(?P<pk>\d+)/$', login_required(
        DeleteView.as_view(
            model=Distributor,
            success_url=("/account/product"))),
            name="delete-distributor"),
)
