from django.contrib import admin
from web_site.Press.models import PressArticle, PressPage


class PressPageAdmin(admin.ModelAdmin):
    list_display = ["__unicode__", "article", "file", "pgnumber"]

    def save_model(self, request, obj, form, change):
        obj.save()

admin.site.register(PressPage, PressPageAdmin)
admin.site.register(PressArticle)
