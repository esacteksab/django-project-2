from django.db import models
from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.translation import ugettext_lazy as _
from django_thumbs.db.models import ImageWithThumbsField

from south.modelsinspector import add_introspection_rules

add_introspection_rules(
    [
        (
            (ImageWithThumbsField, ),
            [],
            {
                "verbose_name": ["verbose_name", {"default": None}],
                "name":         ["name",         {"default": None}],
                "width_field":  ["width_field",  {"default": None}],
                "height_field": ["height_field", {"default": None}],
                "sizes":        ["sizes",        {"default": None}],
            },
        ),
    ],
    ["^django_thumbs.db.models", ])


class PressArticle(models.Model):
    title = models.CharField(_("Title"), max_length=255, null=True, blank=True)
    publication = models.CharField(_("Publication"), max_length=255, null=True,
        blank=True, unique=True)
    year = models.PositiveSmallIntegerField(_("Year"), max_length=4, null=False, blank=False)
    thumbnail = ImageWithThumbsField(_("thumbnail"), upload_to="images/",
        blank=True, null=True, sizes=((125, 125), (250, 250), (640, 427),))

    class Meta:
        ordering = ['-year']

    def __unicode__(self):
        return "'%s' - '%s'" % (self.title, self.publication)


class PressPage(models.Model):
    article = models.ForeignKey(PressArticle)
    file = ImageWithThumbsField(upload_to="images/", sizes=((125, 125), (250, 250), (640, 427),))
    slug = models.SlugField(max_length=50, blank=True)
    pgnumber = models.PositiveSmallIntegerField(null=True,
        blank=True)

    class Meta:
        ordering = ['pgnumber']

    def __unicode__(self):
        return '%s' '%s' '%s' % (self.article, self.file, self.pgnumber)

    @models.permalink
    def get_absolute_url(self, *args, **kwargs):
        return('article-pages', (), {
            'pk': self.article.id
            })

        def save(self, *args, **kwargs):
            self.slug = self.file.name
            super(PressPage, self).save(*args, **kwargs)

        def delete(self, *args, **kwargs):
            self.file.delete(False)
            super(PressPage, self).delete(*args, **kwargs)


class PressPageForm(forms.ModelForm):
    class Meta:
        model = PressPage
        fields = ['file', ]

    def UploadImage(request):
        """Upload Images"""
        if request.method == 'POST':
            form = PressPageForm(request.POST, request.FILES)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.article = form.kwargs['pk']
                obj.pgnumber = 1
                obj.save()
                return HttpResponseRedirect(".")
        else:
            form = PressPageForm()
        template = "presspage_form.html"
        data = {
            'form': form,
        }
        return render_to_response(template, data, context_instance=RequestContext(request))
