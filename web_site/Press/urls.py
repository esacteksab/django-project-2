from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DeleteView, UpdateView
from web_site.Press.models import PressArticle
from web_site.Press.forms import PressArticleForm, PressPageForm
from web_site.Press.views import PagePictureCreateView, PagePictureDeleteView, PageListView, UpdatePageView


urlpatterns = patterns("",
    ### Article admin
    ###
    ### Display Articles
    url(r'^articles$', login_required(ListView.as_view(
        queryset=PressArticle.objects.all().order_by('-year'),
        template_name="account/article_admins.html")),
        name="articles"),
    ### Add Article
    url(r'^article/add/$', 'web_site.Press.views.add_article',
        name="add-article"),
    ### Delete Article
    url(r'^article/delete/(?P<pk>\d+)/$', login_required(DeleteView.as_view(
        model=PressArticle,
        success_url=('/articles'))),
        name="delete-article"),
    ### Edit Article
    url(r'^article/edit/(?P<pk>\d+)/$', login_required(UpdateView.as_view(
        model=PressArticle,
        form_class=PressArticleForm,
        success_url=('/articles'),
        template_name="account/article_admin.html")),
        name="delete-article"),

    ### Article Pages Admin
    ###
    ### Display Article Pages
    url(r'^article/(?P<pk>\d+)/pages/$', login_required(PageListView.as_view(
        template_name="account/article_images.html")),
        name="article-pages"),
    ### Add Page(s) to an Article
    url(r'^article/(?P<pk>\d+)/pages/add/$', login_required(PagePictureCreateView.as_view(),
        {}),
        name="add-article-pages"),
    ### Edit Page to an Article
    url(r'^article/(?P<id>\d+)/pages/page/(?P<pk>\d+)/edit/$', login_required(UpdatePageView.as_view(
        form_class=PressPageForm,
        template_name="account/page_admin.html")),
        name="press-page-edit"),
    ### Delete Page(s) to an Article
    url(r'^article/(?P<id>\d+)/pages/page/(?P<pk>\d+)/delete/$',
        login_required(PagePictureDeleteView.as_view()), {},
            name="press-upload-delete"),
)
