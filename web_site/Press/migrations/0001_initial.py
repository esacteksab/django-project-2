# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PressArticle'
        db.create_table('Press_pressarticle', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('publication', self.gf('django.db.models.fields.CharField')(max_length=255, unique=True, null=True, blank=True)),
            ('year', self.gf('django.db.models.fields.PositiveSmallIntegerField')(max_length=4)),
            ('thumbnail', self.gf('django_thumbs.db.models.ImageWithThumbsField')(name='thumbnail', sizes=((125, 125), (250, 250), (427, 640)), max_length=100, blank=True, null=True)),
        ))
        db.send_create_signal('Press', ['PressArticle'])

        # Adding model 'PressPage'
        db.create_table('Press_presspage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('article', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Press.PressArticle'])),
            ('file', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='file', sizes=((125, 125), (250, 250), (427, 640)))),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, blank=True)),
            ('pgnumber', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('Press', ['PressPage'])


    def backwards(self, orm):
        # Deleting model 'PressArticle'
        db.delete_table('Press_pressarticle')

        # Deleting model 'PressPage'
        db.delete_table('Press_presspage')


    models = {
        'Press.pressarticle': {
            'Meta': {'object_name': 'PressArticle'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication': ('django.db.models.fields.CharField', [], {'max_length': '255', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'thumbnail': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'thumbnail'", 'sizes': '((125, 125), (250, 250), (427, 640))', 'max_length': '100', 'blank': 'True', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.PositiveSmallIntegerField', [], {'max_length': '4'})
        },
        'Press.presspage': {
            'Meta': {'ordering': "['pgnumber']", 'object_name': 'PressPage'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Press.PressArticle']"}),
            'file': ('django_thumbs.db.models.ImageWithThumbsField', [], {'max_length': '100', 'name': "'file'", 'sizes': '((125, 125), (250, 250), (427, 640))'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pgnumber': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'})
        }
    }

    complete_apps = ['Press']