# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'PressArticle.thumbnail'
        db.alter_column('Press_pressarticle', 'thumbnail', self.gf('django_thumbs.db.models.ImageWithThumbsField')(name='thumbnail', sizes=((125, 125), (250, 250), (640, 427)), max_length=100, null=True))

        # Changing field 'PressPage.file'
        db.alter_column('Press_presspage', 'file', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='file', sizes=((125, 125), (250, 250), (640, 427))))

    def backwards(self, orm):

        # Changing field 'PressArticle.thumbnail'
        db.alter_column('Press_pressarticle', 'thumbnail', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, null=True, name='thumbnail', sizes=((125, 125), (250, 250), (427, 640))))

        # Changing field 'PressPage.file'
        db.alter_column('Press_presspage', 'file', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='file', sizes=((125, 125), (250, 250), (427, 640))))

    models = {
        'Press.pressarticle': {
            'Meta': {'object_name': 'PressArticle'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication': ('django.db.models.fields.CharField', [], {'max_length': '255', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'thumbnail': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'thumbnail'", 'sizes': '((125, 125), (250, 250), (640, 427))', 'max_length': '100', 'blank': 'True', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.PositiveSmallIntegerField', [], {'max_length': '4'})
        },
        'Press.presspage': {
            'Meta': {'ordering': "['pgnumber']", 'object_name': 'PressPage'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Press.PressArticle']"}),
            'file': ('django_thumbs.db.models.ImageWithThumbsField', [], {'max_length': '100', 'name': "'file'", 'sizes': '((125, 125), (250, 250), (640, 427))'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pgnumber': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'})
        }
    }

    complete_apps = ['Press']