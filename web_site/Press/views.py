from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils import simplejson
from django.contrib.auth.decorators import login_required
from web_site.Press.forms import PressArticleForm
from web_site.Press.models import PressPage, PressArticle, PressPageForm


def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"


class PagePictureCreateView(CreateView):
    model = PressPage
    template_name = "account/upload.html"
    form_class = PressPageForm

    def form_valid(self, form, **kwargs):
        obj = form.save(commit=False)
        obj.article_id = self.kwargs['pk']
        obj.pgnumber = 1
        obj.save()

        data = [
            {
            'name': obj.file.name,
            'url': obj.file.url,
            'thumbnail_url': obj.file.url,
            'delete_url': reverse('press-upload-delete', kwargs={'id':self.kwargs['pk'], 'pk': obj.id}),
            'delete_type': "DELETE"
            }
        ]

        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class PagePictureDeleteView(DeleteView):
    model = PressPage

    def get_context_data(self, **kwargs):
        context = super(PagePictureDeleteView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']
        return context

    def delete(self, request, *args, **kwargs):
        """
        This does not actually delete the file, only the database record.  But
        that is easy to implement.
        """
        self.object = self.get_object()
        self.object.delete()
        if request.is_ajax():
            response = JSONResponse(True, {}, response_mimetype(self.request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        else:

            return redirect('article-pages', pk=kwargs['id'])


class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self,obj='',json_opts={},mimetype="application/json",*args,**kwargs):
        content = simplejson.dumps(obj,**json_opts)
        super(JSONResponse,self).__init__(content,mimetype,*args,**kwargs)


def publications(request):
    articles = PressArticle.objects.all().order_by('-year')
    pages = PressPage.objects.filter(article__in=articles).order_by('pgnumber')

    return render_to_response('articles.html',
        {'articles': articles, 'pages': pages},
        context_instance=RequestContext(request))


@login_required(login_url='/account/login')
def add_article(request):
    """Add a new Visiting Artist"""
    form = PressArticleForm()
    if request.method == 'POST':
        submit = request.POST.get('cancel', None)
        if submit:
            return HttpResponseRedirect('/account/press/articles')
        else:
            form = PressArticleForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/account/press/articles')
    template = 'account/article_admin.html'
    data = {
        'form': form,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))


@login_required(login_url='/account/login')
def delete_article(self, pk=None):
    """Delete a Press Article"""
    PressArticle.objects.get(pk=pk).delete()

    return HttpResponseRedirect('/account/press/articles')


@login_required(login_url='/account/login')
def edit_article(self, request, pk=None):
    """Edit A Press Article"""
    if request.method == 'POST':
        submit = request.POST.get('cancel', None)
        if submit:
            return HttpResponseRedirect('/account/press/articles')
        else:
            PressArticle.objects.get(pk=pk)
    return HttpResponseRedirect('/account/press/articles')


@login_required(login_url='/account/login')
def add_page(request):
    """Add a new Visiting Artist"""
    form = PressPageForm()
    if request.method == 'POST':
        form = PressPageForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/account/press/articles')
    template = 'adm-article.html'
    data = {
        'form': form,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))


@login_required(login_url='/account/login')
def delete_page(self, pk=None):
    """Delete an Artists"""
    PressPage.objects.get(pk=pk).delete()

    return HttpResponseRedirect('/account/press/articles')


@login_required(login_url='/account/login')
def edit_page(self, pk=None, **kwargs):
    """Edit An Artist"""
    PressPage.objects.get(pk=pk)
    return redirect('article-pages', pk=kwargs['imgid'])


class PageListView(ListView):

    def get_context_data(self, **kwargs):
        context = super(PageListView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']
        return context

    def get_queryset(self):
        return PressPage.objects.filter(article__id=self.kwargs['pk'])


class UpdatePageView(UpdateView):
    model = PressPage

    def get_context_data(self, **kwargs):
        context = super(UpdatePageView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']
        return context

    def update(self, request, *args, **kwargs):

        return redirect('article-pages', pk=kwargs['pk'])


def displayPressArticles(request):
    articles = PressArticle.objects.all().order_by('-year')
    return render_to_response('press-homepage.html',
        {'articles': articles},
        context_instance=RequestContext(request))
