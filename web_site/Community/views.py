from web_site.Community.models import CommunityEvent, CommunityPicture, CommunityPictureForm
from web_site.Community.forms import CommunityEventForm
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, DeleteView, ListView
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import simplejson
from django.core.urlresolvers import reverse


def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"


class CommunityPictureCreateView(CreateView):
    model = CommunityPicture
    template_name = "account/upload.html"
    form_class = CommunityPictureForm

    def form_valid(self, form, **kwargs):
        obj = form.save(commit=False)
        obj.event_id = self.kwargs['pk']
        obj.save()

        data = [
            {
            'name': obj.file.name,
            'url': obj.file.url,
            'thumbnail_url': obj.file.url,
            'delete_url': reverse('community-upload-delete', kwargs={'id':self.kwargs['pk'], 'pk': obj.id}),
            'delete_type': "DELETE"
            }
        ]

        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class CommunityPictureDeleteView(DeleteView):
    model = CommunityPicture

    def get_context_data(self, **kwargs):
        context = super(CommunityPictureDeleteView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']
        return context

    def delete(self, request, *args, **kwargs):
        """
        This does not actually delete the file, only the database record.  But
        that is easy to implement.
        """
        self.object = self.get_object()
        self.object.delete()
        if request.is_ajax():
            response = JSONResponse(True, {}, response_mimetype(self.request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return response
        else:

            return redirect('admin-community-images', pk=kwargs['id'])


class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self,obj='',json_opts={},mimetype="application/json",*args,**kwargs):
        content = simplejson.dumps(obj,**json_opts)
        super(JSONResponse,self).__init__(content,mimetype,*args,**kwargs)


def showCommunity(request):
    events = CommunityEvent.objects.all()
    return render_to_response('community-homepage.html',
        {'events': events},
        context_instance=RequestContext(request))


class MySpecialListView(ListView):

    def get_context_data(self, **kwargs):
        context = super(MySpecialListView, self).get_context_data(**kwargs)
        context['pk'] = self.kwargs['pk']
        return context

    def get_queryset(self):
        return CommunityPicture.objects.filter(event__id=self.kwargs['pk'])


@login_required(login_url='/account/login')
def add_event(request):
    """Add a new Visiting Artist"""
    form = CommunityEventForm()
    if request.method == 'POST':
        submit = request.POST.get('cancel', None)
        if submit:
            return HttpResponseRedirect('/account/community')
        else:
            form = CommunityEventForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/account/community')
    template = 'account/community_admin.html'
    data = {
        'form': form,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))
