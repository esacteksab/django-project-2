from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DeleteView, UpdateView
from web_site.Community.models import CommunityEvent
from web_site.Community.views import MySpecialListView, CommunityPictureCreateView, CommunityPictureDeleteView
from web_site.Community.forms import CommunityEventForm


urlpatterns = patterns("",
    ### Community admin
    url(r'^$', login_required(ListView.as_view(
        model=CommunityEvent,
        template_name="account/community_admins.html")),
        name="community-admin"),
    ### add a community event
    url(r'^add/$', 'web_site.Community.views.add_event',
        name="add-community-event"),
    ### Edit a community event
    url(r'^event/(?P<pk>\d+)/edit/$', login_required(UpdateView.as_view(
        model=CommunityEvent,
        form_class=CommunityEventForm,
        template_name=("account/community_admin.html"),
        success_url=('/account/community')))),
    ### Delete a community event
    url(r'^delete/(?P<pk>\d+)/$', login_required(DeleteView.as_view(
        model=CommunityEvent,
        success_url=("/account/community"),)),
        name="delete-community-event"),



    ### View Community Event Images
    url(r'^event/(?P<pk>\d+)/images/$', login_required(MySpecialListView.as_view(
        template_name="account/community_images.html")),
        name="admin-community-images"),
    ### Add Images to a community event
    url(r'^event/(?P<pk>\d+)/images/add/$', login_required(CommunityPictureCreateView.as_view(),
        {}),
        name="add-community-images"),
    ### Delete community event images
    url(r'^event/(?P<id>\d+)/images/image/(?P<pk>\d+)/delete/$',
        login_required(CommunityPictureDeleteView.as_view()),
            name="community-upload-delete"),
)