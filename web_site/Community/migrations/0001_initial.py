# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CommunityEvent'
        db.create_table('Community_communityevent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('Community', ['CommunityEvent'])

        # Adding model 'CommunityPicture'
        db.create_table('Community_communitypicture', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Community.CommunityEvent'])),
            ('file', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='file', sizes=((125, 125), (250, 250), (427, 640)))),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, blank=True)),
        ))
        db.send_create_signal('Community', ['CommunityPicture'])


    def backwards(self, orm):
        # Deleting model 'CommunityEvent'
        db.delete_table('Community_communityevent')

        # Deleting model 'CommunityPicture'
        db.delete_table('Community_communitypicture')


    models = {
        'Community.communityevent': {
            'Meta': {'object_name': 'CommunityEvent'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'Community.communitypicture': {
            'Meta': {'object_name': 'CommunityPicture'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Community.CommunityEvent']"}),
            'file': ('django_thumbs.db.models.ImageWithThumbsField', [], {'max_length': '100', 'name': "'file'", 'sizes': '((125, 125), (250, 250), (427, 640))'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'})
        }
    }

    complete_apps = ['Community']