# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'CommunityPicture.file'
        db.alter_column('Community_communitypicture', 'file', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='file', sizes=((125, 125), (250, 250), (640, 427))))

    def backwards(self, orm):

        # Changing field 'CommunityPicture.file'
        db.alter_column('Community_communitypicture', 'file', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='file', sizes=((125, 125), (250, 250), (427, 640))))

    models = {
        'Community.communityevent': {
            'Meta': {'object_name': 'CommunityEvent'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'Community.communitypicture': {
            'Meta': {'object_name': 'CommunityPicture'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Community.CommunityEvent']"}),
            'file': ('django_thumbs.db.models.ImageWithThumbsField', [], {'max_length': '100', 'name': "'file'", 'sizes': '((125, 125), (250, 250), (640, 427))'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'})
        }
    }

    complete_apps = ['Community']