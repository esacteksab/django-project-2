from django.contrib import admin
from web_site.Community.models import CommunityEvent, CommunityPicture


admin.site.register(CommunityEvent)
admin.site.register(CommunityPicture)
