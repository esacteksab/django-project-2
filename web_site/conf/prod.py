from .base import *

import os

DEBUG = False
TEMPLATE_DEBUG = False
DEFAULT_FROM_EMAIL = ''
SEND_BROKEN_LINK_EMAILS = True

DATABASES = {
        "default": {
            "ENGINE": 'django.db.backends.postgresql_psycopg2',
            "NAME": 'db',
            "USER": 'user',
            "PASSWORD": 'password',
            "HOST": 'localhost',
            "PORT": '5432'
        }
    }


SITE_ID = 1  # set this to match your Sites setup

MEDIA_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "media")
STATIC_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "static")


FILE_UPLOAD_PERMISSIONS = 0640


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse"
        },
    },
    "formatters": {
        "simple": {
            "format": "%(levelname)s %(message)s"
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "simple"
        },
        "mail_admins": {
            "level": "ERROR",
            "class": "django.utils.log.AdminEmailHandler",
            "include_html": True,
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "INFO",
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        },
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
