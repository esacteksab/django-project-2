from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import UpdateView
from django.contrib.auth.decorators import login_required
from django.views.generic.simple import direct_to_template
from web_site.UserProfile.forms import ArtistPictureForm
from web_site.UserProfile.views import (PictureCreateView, PictureDeleteView,
        PublicGetArtistImages, DisplayAristImagesAdmin)
from web_site.UserProfile.models import Picture
from web_site.Shop.models import Product
from web_site.Shop.views import viewProduct
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns("",
    url(r"^$", direct_to_template, {"template": "homepage.html"},
        name="home"),
    url(r"^shop/$", 'web_site.Carousel.views.ShopHomePage',
        name="shop-home"),
    url(r"^community/$", 'web_site.Community.views.showCommunity',
        name="community"),
    url(r"^artists/(?P<slug>[-\w\d]+)/$", PublicGetArtistImages.as_view(
        template_name="artist-homepage.html"),
        name="artists"),
    url(r"^press/$", 'web_site.Press.views.displayPressArticles',
        name="press"),
    url(r"^news/$", 'web_site.News.views.displayNewsItems',
        name="news"),
    url(r'^product/$', 'web_site.Shop.views.displayMerchandise',
        name="merchandise"),
    url(r'^product/(?P<slug>[-\w\d]+)/$', viewProduct.as_view(
        model=Product,
        template_name="merchandise-item-homepage.html"),
        name="product-page"),

    ### User Images accountin
    url(r'^account/images/$', login_required(
        DisplayAristImagesAdmin.as_view(
        template_name="account/account_images.html")),
        name="manage-artist-images"),

    ### Add Account Image
    url(r'^account/images/add/$', login_required(
        PictureCreateView.as_view()),
        name="upload-artist-images"),
    ### Delete Account Image
    url(r'^account/images/delete/(?P<pk>\d+)$',
        login_required(PictureDeleteView.as_view()),
        name="delete-artist-images"),

    ### Edit an Artist's Image
    url(r'^account/images/edit/(?P<pk>\d+)$', login_required(
        UpdateView.as_view(
        model=Picture,
        form_class=ArtistPictureForm,
        success_url=("/account/images"),
        template_name="account/artist_images_admin.html")),
        name="admin-artist-image-edit"),




    url(r"^account/community/", include("web_site.Community.urls")),
    url(r"^account/press/", include("web_site.Press.urls")),
    url(r"^account/news/", include("web_site.News.urls")),
    url(r"^account/artists/", include("web_site.UserProfile.urls")),
    url(r"^account/product/", include('web_site.Shop.urls')),
    url(r"^account/carousel/", include('web_site.Carousel.urls')),



    ### Redactor.js WYSIWYG Editor
    ###
    url(r"^uploads/$", "web_site.News.views.upload_photos",
        name="upload_photos"),
    url(r"^recent/$", "web_site.News.views.recent_photos",
        name="recent_photos"),


    ### admin
    url(r"^admin/", include(admin.site.urls)),

    url(r"^account/", include("account.urls")),

)

import os
urlpatterns += patterns('',
    (r'^media/(.*)$',
        'django.views.static.serve',
            {'document_root':
            os.path.join(os.path.abspath(os.path.dirname(__file__)),
                'media')}),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
