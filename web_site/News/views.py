import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from web_site.News.models import NewsArticleImage, NewsArticle
from web_site.News.forms import NewsForm


@csrf_exempt
@require_POST
@login_required
def upload_photos(request):
    images = []
    for f in request.FILES.getlist("file"):
        obj = NewsArticleImage.objects.create(upload=f)
        images.append({"filelink": obj.upload.url})
    return HttpResponse(json.dumps(images), mimetype="application/json")


@login_required
def recent_photos(request):
    images = [
    {"thumb": obj.upload.url, "image": obj.upload.url}
        for obj in NewsArticleImage.objects.all().order_by("-date_created")[:20]
    ]
    return HttpResponse(json.dumps(images), mimetype="application/json")


@login_required
def add_news_item(request):
    """ Add a new news item """
    form = NewsForm()
    if request.method == 'POST':
        submit = request.POST.get('cancel', None)
        if submit:
            return HttpResponseRedirect('/account/news')
        else:
            form = NewsForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/account/news')
    template = 'account/news_admin.html'
    data = {
        'form': form,
    }

    return render_to_response(template, data, context_instance=RequestContext(request))


def displayNewsItems(request):
    news = NewsArticle.objects.all()
    template = 'news-homepage.html'
    data = {
        'news': news,
    }
    return render(request, template, data)
