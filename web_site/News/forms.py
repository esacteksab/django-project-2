from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from crispy_forms.bootstrap import *
from web_site.News.models import NewsArticle


class NewsForm(ModelForm):
    """A form to allow the creation of a new article"""
    class Meta:
        model = NewsArticle
        fields = ['title', 'content']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = 'add-article'
        self.helper.form_class = 'well form-inline'
        self.helper.form_action = ''
        self.helper.layout = Layout(

            Field('title', css_class="input-large", placeholder="Title"),
            Field('content', type="textarea", id="redactor_content", name="content"),

        FormActions(
            Submit('submit', "Submit", css_class='btn'),
            Submit('cancel', "Cancel", css_class='btn'),
        )
    )
        super(NewsForm, self).__init__(*args, **kwargs)
