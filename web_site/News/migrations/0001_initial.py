# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'NewsArticle'
        db.create_table('News_newsarticle', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('News', ['NewsArticle'])

        # Adding model 'NewsArticleImage'
        db.create_table('News_newsarticleimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('upload', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='upload', sizes=((125, 125), (250, 250), (427, 640)))),
            ('is_image', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('News', ['NewsArticleImage'])


    def backwards(self, orm):
        # Deleting model 'NewsArticle'
        db.delete_table('News_newsarticle')

        # Deleting model 'NewsArticleImage'
        db.delete_table('News_newsarticleimage')


    models = {
        'News.newsarticle': {
            'Meta': {'object_name': 'NewsArticle'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'News.newsarticleimage': {
            'Meta': {'object_name': 'NewsArticleImage'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_image': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'upload': ('django_thumbs.db.models.ImageWithThumbsField', [], {'max_length': '100', 'name': "'upload'", 'sizes': '((125, 125), (250, 250), (427, 640))'})
        }
    }

    complete_apps = ['News']