# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'NewsArticleImage.upload'
        db.alter_column('News_newsarticleimage', 'upload', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='upload', sizes=((125, 125), (250, 250), (640, 427))))

    def backwards(self, orm):

        # Changing field 'NewsArticleImage.upload'
        db.alter_column('News_newsarticleimage', 'upload', self.gf('django_thumbs.db.models.ImageWithThumbsField')(max_length=100, name='upload', sizes=((125, 125), (250, 250), (427, 640))))

    models = {
        'News.newsarticle': {
            'Meta': {'object_name': 'NewsArticle'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'News.newsarticleimage': {
            'Meta': {'object_name': 'NewsArticleImage'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_image': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'upload': ('django_thumbs.db.models.ImageWithThumbsField', [], {'max_length': '100', 'name': "'upload'", 'sizes': '((125, 125), (250, 250), (640, 427))'})
        }
    }

    complete_apps = ['News']