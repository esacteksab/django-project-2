from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DeleteView, UpdateView


from web_site.News.models import NewsArticle
from web_site.News.forms import NewsForm


urlpatterns = patterns("",
    ### News Admin
    ###
    ### View News Articles
    url(r'^$', login_required(ListView.as_view(
        model=NewsArticle,
        template_name="account/news_admins.html")),
        name="admin-display-news"),

    ### Create News Article
    url(r'^add/$', 'web_site.News.views.add_news_item',
        name="admin-add-news"),

    ### Edit News Article -- This may need to be CBV to support cancel
    url(r'^(?P<pk>\d+)/edit/$', login_required(UpdateView.as_view(
        model=NewsArticle,
        form_class=NewsForm,
        success_url=("/account/news"),
        template_name="account/news_admin.html")),
        name="admin-edit-news"),

    ### Delete News Article
    url(r'^(?P<pk>\d+)/delete/$', login_required(DeleteView.as_view(
        model=NewsArticle,
        success_url=('/account/news'))),
        name="admin-delete-news"),

)