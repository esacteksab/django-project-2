from django.db import models
from django_thumbs.db.models import ImageWithThumbsField
from south.modelsinspector import add_introspection_rules


add_introspection_rules(
    [
        (
            (ImageWithThumbsField, ),
            [],
            {
                "verbose_name": ["verbose_name", {"default": None}],
                "name":         ["name",         {"default": None}],
                "width_field":  ["width_field",  {"default": None}],
                "height_field": ["height_field", {"default": None}],
                "sizes":        ["sizes",        {"default": None}],
            },
        ),
    ],
    ["^django_thumbs.db.models", ])


class NewsArticle(models.Model):
    title = models.CharField(max_length=255, blank=False)
    content = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']

    def __unicode__(self):
        return '%s' '%s' % (self.title, self.content)


class NewsArticleImage(models.Model):
    upload = ImageWithThumbsField(upload_to="uploads/%Y/%m/%d/", sizes=((125, 125), (250, 250), (640, 427),))
    is_image = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.upload
